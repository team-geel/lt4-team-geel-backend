package com.schmaak.Repo;

import com.schmaak.Models.Consumable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumableRepo extends JpaRepository<Consumable, Long> { }
