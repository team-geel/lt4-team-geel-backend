package com.schmaak.Repo;

import com.schmaak.Models.Order;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderedConsumablesRepo extends JpaRepository<Order, Long> {
}
