package com.schmaak.Exceptions;

public class ConsumableNotCompleteException extends Exception{
    public ConsumableNotCompleteException(String message) {
        super(message);
    }
}
