package com.schmaak.Exceptions;

public class ConsumableNotFoundException extends Exception{
    public ConsumableNotFoundException(String message) {
        super(message);
    }
}
