package com.schmaak;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schmaak.Models.Consumable;
import com.schmaak.service.Consumable.ConsumableService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

//write JSON to db

//    @Bean
//    CommandLineRunner runner(ConsumableService consumableService) {
//        return args -> {
//            // read json and write to db
//            ObjectMapper mapper = new ObjectMapper();
//            TypeReference<List<Consumable>> typeReference = new TypeReference<>() {
//            };
//            InputStream inputStream = TypeReference.class.getResourceAsStream("/json/consumables.json");
//            try {
//                List<Consumable> consumables = mapper.readValue(inputStream, typeReference);
//                consumableService.saveConsumables(consumables);
//                System.out.println("Consumables saved!");
//
//            } catch (IOException e) {
//                System.out.println("unable to save consumables: " + e.getMessage());
//            }
//        };
//    }
}
