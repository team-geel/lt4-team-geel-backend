package com.schmaak.service.OrderedConsumables;

import com.schmaak.Models.Order;
import com.schmaak.Repo.OrderedConsumablesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderedConsumablesImpl implements OrderedConsumablesService{
    @Autowired
    OrderedConsumablesRepo orderedConsumablesRepo;


    @Override
    public Optional<Order> findByConsumableId(long id) {
        return orderedConsumablesRepo.findById(id);
    }

    @Override
    public List<Order> getOrderedConsumable() {
        return orderedConsumablesRepo.findAll();
    }

    @Override
    public Order updateOrderedConsumableStatusById(long id, Order order) {
        Order existingOrder = orderedConsumablesRepo.findById(id).orElse(null);

        assert existingOrder != null;
        existingOrder.setStatus(order.getStatus());
        existingOrder.setTimestamp(order.getTimestamp());

        return orderedConsumablesRepo.save(existingOrder);
    }
}
