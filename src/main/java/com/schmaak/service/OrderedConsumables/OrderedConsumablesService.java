package com.schmaak.service.OrderedConsumables;

import com.schmaak.Models.Order;

import java.util.List;
import java.util.Optional;

public interface OrderedConsumablesService {
//    Order saveOrderedConsumable(Order order);
//    List<OrderedConsumables> getOrderedConsumable();

    Optional<Order> findByConsumableId(long id);

    List<Order> getOrderedConsumable();

    Order updateOrderedConsumableStatusById(long id, Order order);
}
