package com.schmaak.service.Consumable;

import com.schmaak.Exceptions.ConsumableNotCompleteException;
import com.schmaak.Exceptions.ConsumableNotFoundException;
import com.schmaak.Models.Consumable;

import java.util.List;
import java.util.Optional;

public interface ConsumableService {
    List<Consumable> saveConsumables(List<Consumable> entities) throws ConsumableNotFoundException;

    Consumable saveConsumable(Consumable order) throws ConsumableNotCompleteException;

    Consumable updateConsumable(long id, Consumable order);

    List<Consumable> getConsumable();

    Optional<Consumable> findConsumableById(long id) throws ConsumableNotFoundException;

    void delete(long id);
}
