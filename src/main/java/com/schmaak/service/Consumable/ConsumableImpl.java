package com.schmaak.service.Consumable;

import com.schmaak.Exceptions.ConsumableNotCompleteException;
import com.schmaak.Exceptions.ConsumableNotFoundException;
import com.schmaak.Models.Consumable;
import com.schmaak.Repo.ConsumableRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConsumableImpl implements ConsumableService{
    @Autowired
    private ConsumableRepo consumableRepo;

    @Override
    public Consumable saveConsumable(Consumable order) throws ConsumableNotCompleteException {
        if (!order.getName().isBlank() ||
            !order.getCategory().isBlank() ||
            !order.getType().isBlank() ||
            !order.getImage().isBlank()
        ){
            return consumableRepo.save(order);
        } else {
            throw new ConsumableNotCompleteException("field must not be empty");
        }
    }

    public List<Consumable> saveConsumables(List<Consumable> entities) throws ConsumableNotFoundException {
        if (!entities.isEmpty()){
            return consumableRepo.saveAll(entities);
        } else {
            throw new ConsumableNotFoundException("Consumables not found");
        }
    }

    @Override
    public Consumable updateConsumable(long id, Consumable consumable) {
        Consumable existingConsumable = consumableRepo.findById(id).orElse(null);
        assert existingConsumable != null;
        existingConsumable.setCategory(consumable.getCategory());
        existingConsumable.setDescription(consumable.getDescription());
        existingConsumable.setImage(consumable.getImage());
        existingConsumable.setName(consumable.getName());
        existingConsumable.setType(consumable.getType());
        existingConsumable.setPrice(consumable.getPrice());

        return consumableRepo.save(existingConsumable);
    }

    @Override
    public List<Consumable> getConsumable() {
        return consumableRepo.findAll();
    }

    @Override
    public Optional<Consumable> findConsumableById(long id) throws ConsumableNotFoundException {
        Optional<Consumable> consumable = consumableRepo.findById(id);
        if (consumable.isPresent()){
            return consumable;
        }else {
            throw new ConsumableNotFoundException("consumable not found with id: " + id);
        }
    }


    @Override
    public void delete(long id) {
        consumableRepo.deleteById(id);
    }


}
