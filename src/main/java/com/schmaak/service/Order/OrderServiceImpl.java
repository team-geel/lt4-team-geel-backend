package com.schmaak.service.Order;

import com.schmaak.Models.Order;
import com.schmaak.Repo.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepo orderRepository;

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return orderRepository.saveAndFlush(order);
    }

    @Override
    public Order updateOrderStatus(Order order, long id) {
        Order existingOrder = orderRepository.getReferenceById(id);
        existingOrder.setStatus(order.getStatus());
        existingOrder.setTimestamp(order.getTimestamp());
        return orderRepository.save(existingOrder);
    }

    @Override
    public Optional<Order> getOrder(long id) {
        return orderRepository.findById(id);
    }

    @Override
    public String delete(long id) {
        orderRepository.deleteById(id);
        return "Order is deleted";
    }

    @Override
    public List<Order> findOrdersByTable(int tableId) {
        return orderRepository.findAllByTableNumber(tableId);
    }

}
