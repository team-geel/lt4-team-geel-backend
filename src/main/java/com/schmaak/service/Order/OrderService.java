package com.schmaak.service.Order;

import com.schmaak.Models.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    Order saveOrder(Order order);
    Order updateOrder(Order order);

    Order updateOrderStatus(Order order, long id);
    Optional<Order> getOrder(long id);
    String delete(long id);


    List<Order> findOrdersByTable(int tableId);
}
