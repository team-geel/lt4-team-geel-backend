package com.schmaak.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ORDER_TABLE")
public class Order {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private int tableNumber;

    @Column
    private String orderComment;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalDateTime timestamp = LocalDateTime.now();

    @Column
    private String status = "In voorbereiding";

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrderedConsumables> orderList;


    public Order() {
    }

    public Order(int tableNumber, String orderComment, List<OrderedConsumables> orderList) {
        this.tableNumber = tableNumber;
        this.orderComment = orderComment;
        this.orderList = orderList;
        this.timestamp = getTimestamp();
        this.status = getStatus();
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getOrderComment() {
        return orderComment;
    }

    public void setOrderComment(String orderComment) {
        this.orderComment = orderComment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderedConsumables> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderedConsumables> orderList) {
        this.orderList = orderList;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    public LocalDateTime getTimestamp() {
        return timestamp;
    }
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }
}
