package com.schmaak.Models;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table
public class OrderedConsumables {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    private int quantity;

    @Column
    private int price;

    public OrderedConsumables() {
    }

    public OrderedConsumables(String name, int quantity, int price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
