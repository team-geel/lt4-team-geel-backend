package com.schmaak.Models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "CONSUMABLE_TBL")
public class Consumable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumable_name")
    @NotBlank
    private String name;

    @Column
    private String description;

    @Column
    @NotNull
    private float price;

    @Column
    @NotBlank
    private String category;

    @Column
    @NotBlank
    private String type;

    @Column
    @NotBlank
    private String image;

    public Consumable() {
    }

    public Consumable(String name, String description, float price, String category, String type, String image) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.type = type;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCategory() {return category;}

    public void setCategory(String category) {this.category = category;}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

