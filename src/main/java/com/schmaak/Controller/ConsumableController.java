package com.schmaak.Controller;

import com.schmaak.Exceptions.ConsumableNotCompleteException;
import com.schmaak.Exceptions.ConsumableNotFoundException;
import com.schmaak.Models.Consumable;
import com.schmaak.service.Consumable.ConsumableService;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/consumable")
@CrossOrigin(origins = "http://localhost:3000")
public class ConsumableController {
    @Autowired
    private ConsumableService consumableService;

    @PostMapping
    public Consumable addConsumable(@RequestBody @Valid Consumable entity) throws ConsumableNotCompleteException {
        return consumableService.saveConsumable(entity);
    }

    @PostMapping("/many")
    public List<Consumable> addConsumables(@RequestBody List<Consumable> entities) throws ConsumableNotFoundException {
        return consumableService.saveConsumables(entities);

    }
    @GetMapping
    public List<Consumable> ConsumableList() {
        return consumableService.getConsumable();
    }

    @GetMapping("/{id}")
    public Optional<Consumable> findConsumableById(@PathVariable long id ) throws ConsumableNotFoundException {
        return consumableService.findConsumableById(id);
    }

    @PutMapping("/{id}")
    public Consumable update(@RequestBody Consumable entity, @PathVariable long id) {
        return consumableService.updateConsumable(id, entity);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable long id) {
        consumableService.delete(id);
        return "Consumable is deleted";
    }
}
