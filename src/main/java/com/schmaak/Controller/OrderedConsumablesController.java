package com.schmaak.Controller;

import com.schmaak.Models.Order;
import com.schmaak.service.OrderedConsumables.OrderedConsumablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orderedConsumables")
@CrossOrigin(origins = "http://localhost:3000")

public class OrderedConsumablesController {
    @Autowired
    OrderedConsumablesService orderedConsumablesService;

    @GetMapping("/all")
    public List<Order> getOrderedConsumables() {
        return orderedConsumablesService.getOrderedConsumable();
    }

    @RequestMapping(value = "/{id}")
    public Optional<Order> getOrderedConsumablesList(@PathVariable long id){
        return orderedConsumablesService.findByConsumableId(id);
    }

    @PutMapping(value = "/update/{id}")
    public Order updateOrderStatus(@RequestBody Order entity, @PathVariable long id) {
        return  orderedConsumablesService.updateOrderedConsumableStatusById(id, entity);
    }

}
