package com.schmaak.Controller;

import com.schmaak.Models.Order;
import com.schmaak.service.Order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/order")
@CrossOrigin(origins = "http://localhost:3000")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public Order addOrder(@RequestBody Order entity) {
        return orderService.saveOrder(entity);
    }

    @GetMapping("/{id}")
    public Optional<Order> getById(@PathVariable long id) {
        return orderService.getOrder(id);
    }

    @GetMapping(path = "/table")
    public List<Order> findOrdersByTable(@RequestParam int tableId){

        return orderService.findOrdersByTable(tableId);
    }


    @PutMapping
    public Order update(@RequestBody Order entity) {
        return orderService.updateOrder(entity);
    }

    @PutMapping("/update/{id}")
    public Order updateStatus(@RequestBody Order entity, @PathVariable long id) {
        return orderService.updateOrderStatus(entity, id);
    }

    @DeleteMapping
    public void  delete(@RequestParam long id) {
        orderService.delete(id);
    }
}

