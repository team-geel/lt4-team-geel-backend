package com.schmaak.Controller;

import com.schmaak.Models.Consumable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestH2Repository extends JpaRepository<Consumable, Long> {
}
