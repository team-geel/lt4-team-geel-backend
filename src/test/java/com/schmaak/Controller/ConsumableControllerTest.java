package com.schmaak.Controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.schmaak.Models.Consumable;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ConsumableControllerTest {
    @LocalServerPort
    private int port;

    private String baseUrl = "http://localhost";

    private static RestTemplate restTemplate;

    @Autowired
    private TestH2Repository h2Repository;

    @BeforeAll
    public static void init() {
        restTemplate = new RestTemplate();
    }

    @BeforeEach
    public void setUp() {baseUrl = baseUrl.concat(":").concat(port + "").concat("/consumable");}
    @BeforeEach
    public void cleanup() throws IOException {h2Repository.deleteAll();}

    @Test
    void testAddConsumable() {
        // Create a new Consumable object and save it to the database
        Consumable consumable = new Consumable("testConsumable", "testconsumable", 6, "testCategory", "testType", "test image");
        Consumable response = restTemplate.postForObject(baseUrl, consumable, Consumable.class);
        int responseSize = Objects.requireNonNull(restTemplate.getForObject(baseUrl, List.class)).size();

        // Find the consumable by id in the database and retrieve it
        assert response != null;
        long consumableId = response.getId();
        Consumable retrievedConsumable = h2Repository.findById(consumableId).get();

        //assert that the created fields of the consumable is equal to the consumable saved in the database
        assertEquals(consumable.getName(), retrievedConsumable.getName());
        assertEquals(consumable.getCategory(), retrievedConsumable.getCategory());
        assertEquals(consumable.getType(), retrievedConsumable.getType());
        assertEquals(consumable.getDescription(), retrievedConsumable.getDescription());
        assertEquals(consumable.getPrice(), retrievedConsumable.getPrice());
        assertEquals(consumable.getImage(), retrievedConsumable.getImage());
        assertEquals(responseSize, h2Repository.findAll().size());
    }

    @Test
    void testConsumableList() {
        Consumable consumable = new Consumable("testConsumable2", "description2", 6, "testcategory2", "type2", "image2");
        h2Repository.save(consumable);
        List responseSize = restTemplate.getForObject(baseUrl, List.class);
        assert responseSize != null;
        assertEquals(h2Repository.findAll().size(), responseSize.size());

    }

    @Test
    void testAddConsumablesList() {
        // Create multiple consumables and add them to a List
        List<Consumable> consumables = new ArrayList<>();
        consumables.add(new Consumable("testConsumable1", "testconsumable1", 6, "testCategory", "testType", "test image"));
        consumables.add(new Consumable("testConsumable2", "testconsumable2", 6, "testCategory", "testType", "test image"));
        consumables.add(new Consumable("testConsumable3", "testconsumable2", 6, "testCategory", "testType", "test image"));

        //Post the list with multiple consumables to save it in the database
        List response = restTemplate.postForObject(baseUrl + "/many", consumables, List.class);

        //Retrieve the list with consumables from the database
        List retrievedConsumables = h2Repository.findAll();

        // Testing the response with the retrieved items from the database
        assertNotNull(response);
        assertNotNull(retrievedConsumables);
        assertEquals(response.size(), retrievedConsumables.size());

    }

    @Test
    void update() {
        // Create a new Consumable object and save it to the database
        Consumable consumable = new Consumable("testConsumable", "testconsumable", 6, "testCategory", "testType", "test image");
        h2Repository.save(consumable);
        long id = consumable.getId();

        // Update the Consumable object
        consumable.setName("updatedName");
        consumable.setDescription("updatedDescription");
        consumable.setPrice(12);
        consumable.setCategory("updatedCategory");
        consumable.setType("updatedType");
        consumable.setImage("updatedImage");

        // Send a PUT request to the /consumable/{id} endpoint to update the Consumable object
        restTemplate.put(baseUrl + "/" + id, consumable);

        // Retrieve the updated Consumable object from the database and verify that the changes were made
        Consumable updatedConsumable = h2Repository.findById(id).get();
        System.out.println(updatedConsumable);
        assertEquals("updatedName", updatedConsumable.getName());
        assertEquals("updatedDescription", updatedConsumable.getDescription());
        assertEquals(12, updatedConsumable.getPrice());
        assertEquals("updatedCategory", updatedConsumable.getCategory());
        assertEquals("updatedType", updatedConsumable.getType());
        assertEquals("updatedImage", updatedConsumable.getImage());
    }

    @Test
    void delete() {
        //save the consumable to the database
        Consumable consumable = new Consumable("testConsumable2", "description2", 6, "testcategory2", "type2", "image2");
        h2Repository.save(consumable);

        // get the consumable id
        long id = consumable.getId();

        // Do a Delete request to delete the consumable with the exact id from the database
        restTemplate.delete(baseUrl + "/" + id);

        List responseSize = restTemplate.getForObject(baseUrl, List.class);

        assert responseSize != null;
        assertEquals(responseSize.size(), h2Repository.findAll().size());
    }
}